
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
Class Crud_model extends CI_MODEL
{
    public function __construct()
    {
 
        parent::__construct();
 
    }
 
    //obtenemos los anuncios
    public function get_users()
    {
 
		$query = $this->db->query("
SELECT sbd_categorias.descripcion , sbd_anuncios.id,sbd_anuncios.nom,sbd_poblacion.poblacion ,sbd_anuncios.eslogan,sbd_anuncios.nom_seo,sbd_anuncios.eslogan,sbd_anuncios.cp,sbd_img_anuncios.ruta,sbd_img_anuncios.ruta_p,sbd_provincia.provincia FROM sbd_anuncios,sbd_img_anuncios ,sbd_anuncios_categorias,sbd_poblacion,sbd_provincia,sbd_categorias WHERE sbd_img_anuncios.id_anuncio = sbd_anuncios.id AND sbd_img_anuncios.img_buscador=1 AND sbd_provincia.id=sbd_poblacion.idprovincia AND sbd_poblacion.postal = sbd_anuncios.cp AND sbd_anuncios_categorias.id_categoria = sbd_categorias.id AND sbd_anuncios.id = sbd_anuncios_categorias.id_anuncio AND sbd_anuncios.id IN (779,756,247,47) AND activo = 1 GROUP BY nom ORDER BY ID DESC 		");
        if($query->num_rows() > 0)
        {
 
            return $query->result();
 
        }
 
    }
	//obtenemos los anuncios 2
    public function get_anuncios()
    {
 
		$query = $this->db->query("
			SELECT sbd_categorias.descripcion , sbd_anuncios.id,sbd_anuncios.nom,sbd_poblacion.poblacion ,sbd_anuncios.eslogan,sbd_anuncios.nom_seo,sbd_anuncios.eslogan,sbd_anuncios.cp,sbd_img_anuncios.ruta,sbd_img_anuncios.ruta_p,sbd_provincia.provincia FROM sbd_anuncios,sbd_img_anuncios ,sbd_anuncios_categorias,sbd_poblacion,sbd_provincia,sbd_categorias WHERE sbd_img_anuncios.id_anuncio = sbd_anuncios.id AND sbd_img_anuncios.img_buscador=1 AND sbd_provincia.id=sbd_poblacion.idprovincia AND sbd_poblacion.postal = sbd_anuncios.cp AND sbd_anuncios_categorias.id_categoria = sbd_categorias.id AND sbd_anuncios.id = sbd_anuncios_categorias.id_anuncio AND sbd_anuncios.id IN (106,828,129,718,813,127,585,528) AND activo = 1 GROUP BY nom ORDER BY ID DESC 
		");
        if($query->num_rows() > 0)
        {
 
            return $query->result();
 
        }
 
    }
	
	//obtenemos los anuncios 2
    public function get_recomendados()
    {
 
	$query = $this->db->query("
			SELECT DISTINCT sbd_categorias.descripcion , sbd_anuncios.id,sbd_anuncios.nom,sbd_poblacion.poblacion ,sbd_anuncios.eslogan,sbd_anuncios.nom_seo,sbd_anuncios.eslogan,sbd_anuncios.cp,sbd_img_anuncios.ruta,sbd_img_anuncios.ruta_p,sbd_provincia.provincia FROM sbd_anuncios,sbd_img_anuncios ,sbd_anuncios_categorias,sbd_poblacion,sbd_provincia,sbd_categorias WHERE sbd_img_anuncios.id_anuncio = sbd_anuncios.id AND sbd_img_anuncios.img_buscador=1 AND sbd_provincia.id=sbd_poblacion.idprovincia AND sbd_poblacion.postal = sbd_anuncios.cp AND sbd_anuncios_categorias.id_categoria = sbd_categorias.id AND sbd_anuncios.id = sbd_anuncios_categorias.id_anuncio AND sbd_anuncios.id IN (10,606,15,12,14,148) AND activo = 1 ORDER BY ID DESC
		");
		if($query->num_rows() > 0)
        {
 
            return $query->result();
 
        }
 
    }
	
	//obtenemos los anuncios 2
    public function get_categorias()
    {
	
		 
 
		$query = $this->db->query("SELECT * FROM sbd_categorias ORDER BY descripcion ASC");
        if($query->num_rows() > 0)
        {
 
            return $query->result();
 
        }
 
    }
	
	//obtenemos los anuncios 2
    public function get_provincias()
    {
 
		$query = $this->db->query("SELECT * FROM sbd_provincia where id!=53 ORDER BY provincia ASC");
        if($query->num_rows() > 0)
        {
 
            return $query->result();
 
        }
 
    }
	
//obtenemos los anuncios 2
    public function get_consultas($categoria,$provincia)
    { 
			
			$query = $this->db->query("
			SELECT 
				sbd_anuncios.id,
				sbd_anuncios.nom,
				sbd_anuncios.eslogan,
				sbd_provincia.provincia,
				sbd_anuncios.nom_seo,
				sbd_anuncios.eslogan, 
				sbd_anuncios.cp, 
				sbd_img_anuncios.ruta_p,
				sbd_poblacion.poblacion
			FROM 
				sbd_anuncios,
				sbd_provincia,
				sbd_img_anuncios,
				sbd_anuncios_provincias,
				sbd_anuncios_categorias,
				sbd_categorias,
				sbd_poblacion
			WHERE sbd_categorias.descripcionseo = '$categoria'
			AND sbd_anuncios.cp = sbd_poblacion.postal
			AND sbd_provincia.provinciaseo = '$provincia'
			AND sbd_anuncios_provincias.id_provincia = sbd_provincia.id
			AND sbd_anuncios_provincias.id_anuncio =sbd_anuncios.id 
			AND sbd_anuncios_categorias.id_categoria = sbd_categorias.id 
			AND sbd_anuncios_categorias.id_anuncio = sbd_anuncios.id 
			AND sbd_img_anuncios.id_anuncio = sbd_anuncios.id 
			AND sbd_img_anuncios.img_buscador = 1 
			AND activo = 1 
			GROUP BY sbd_anuncios.id
			ORDER BY sbd_anuncios.caducidad
		");
		if($query->num_rows() > 0)
        {
		
 
            return $query->result();
 
        }
 
    }
	
		
//obtenemos los anuncios 2
    public function get_info_anuncio($id)
    { 			
		$query = $this->db->query("
			SELECT * FROM sbd_img_anuncios WHERE id_anuncio=$id AND img_ficha_g=1 
		");
		if($query->num_rows() > 0)
        {
		 
            return $query->result();
 
        }
 
    }
	//obtenemos los anuncios 2
	///SELECT * FROM sbd_anuncios WHERE id=10 
	//
    public function get_info($id)
    { 			
		if ($id === 0)
		{
		$query = $this->db->get_where('sbd_anuncios',array('completed' => 0));
		return $query->result_array();
		}
		$query = $this->db->get_where('sbd_anuncios', array('id' => $id));
		return $query->row_array();
		
 
    }
	 public function get_banner($id)
    {
		if ($id === 0)
		{
		$query = $this->db->get_where('sbd_img_anuncios',array('completed' => 0));
		return $query->result_array();
		}
		$query = $this->db->get_where('sbd_img_anuncios', array('id_anuncio' => $id,'img_banner'=>1));
		return $query->row_array();
 
    }

	

}