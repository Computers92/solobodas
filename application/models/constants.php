<?php
/*
 * Fitxer de constants
 */

// Taules de la BBDD
define ( "DB_TBL_PREFIX", "sbd_" );
define ( "DB_TBL_ANUNCIOS", DB_TBL_PREFIX . "anuncios " );
define ( "DB_TBL_ANUNCIOS_RECOMENDADOS", DB_TBL_PREFIX . "anuncios_recomendados " );
define ( "DB_TBL_ANUNCIOS_PROVINCIAS", DB_TBL_PREFIX . "anuncios_provincias " );
define ( "DB_TBL_ANUNCIOS_RRSS", DB_TBL_PREFIX . "anuncios_redes_sociales " );
define ( "DB_TBL_CATEGORIAS", DB_TBL_PREFIX . "categorias " );
define ( "DB_TBL_IMG_ANUNCIOS", DB_TBL_PREFIX . "img_anuncios " );
define ( "DB_TBL_POBLACION", DB_TBL_PREFIX . "poblacion " );
define ( "DB_TBL_PROVINCIA", DB_TBL_PREFIX . "provincia " );
define ( "DB_TBL_USUARIOS", DB_TBL_PREFIX . "usuarios " );
define ( "DB_TBL_NOVIOS", DB_TBL_PREFIX . "novios " );
define ( "DB_TBL_USUARIOS_TIPO", DB_TBL_PREFIX . "usuarios_tipo " );
define ( "DB_TBL_LOG", DB_TBL_PREFIX . "log " );
define ( "DB_TBL_ACCIONES", DB_TBL_PREFIX . "acciones " );
define ( "DB_TBL_REDES_SOCIALES", DB_TBL_PREFIX . "redes_sociales " );
define ( "DB_TBL_REPORTAJES", DB_TBL_PREFIX . "reportajes " );
define ( "DB_TBL_FERIAS", DB_TBL_PREFIX . "ferias " );
define ( "DB_TBL_ANUNCIOS_CATEGORIAS", DB_TBL_PREFIX . "anuncios_categorias " );
define ( "DB_TBL_ANUNCIOS_COSTES", DB_TBL_PREFIX . "costes_anuncio " );
?>