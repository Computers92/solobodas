<?php
/*
 * Exemple per C&P 
 * function x() { 
 * 		$return = ""; // Valorn que retornarem 
 * 		$query = "SELECT * FROM " . DB_TBL_*; 
 * 
 * 		$result = sql ( $query ); 
 * 
 * 		// Si és correcte torna la llista de categories, si no retorna un error 
 * 		$return = $result ["result"]; 
 * 
 * 		return $return; 
 * }
 */

/*
 * Funcions de les categories 
 */

// Obtenir la llista de categories
function get_categories() {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT * FROM " . DB_TBL_CATEGORIAS;
	$query .= "ORDER BY descripcion ";
	
	$result = sql ( $query );
	
	// Si és correcte torna la llista de categories, si no retorna un error
	$return = $result ["result"];
	//var_dump($return);
	return $return;
}

// Obtenir una categoria a partir del seu ID
function get_category_by_id($category_id) {
	$return = ""; // Valor que retornarem
	
	$query = "SELECT descripcion FROM " . DB_TBL_CATEGORIAS;
	$query .= "WHERE id = $category_id";
	
	$result = sql ( $query );
	
	// Si és correcte torna el nom de la categoria, si no retorna un error
	if ($result ["ok"]) {
		$return = $result ["result"] [0] ["descripcion"];
	} else {
		$return = $result ["result"];
	}
	return $return;
}

//Obtenir el nom d'una categoria a partir del SEO
function get_category_by_seo($seo){
	$return = ""; // Valor que retornarem
	
	$query = "SELECT descripcion FROM " . DB_TBL_CATEGORIAS;
	$query .= "WHERE descripcionseo = \"$seo\"";
	
	$result = sql ( $query );
	
	// Si és correcte torna el nom de la categoria, si no retorna un error
	if ($result ["ok"]) {
		$return = $result ["result"] [0] ["descripcion"];
	} else {
		$return = $result ["result"];
	}
	return $return;
}

/*
 * Funccions de anuncis
 */

// Obtenir un anunci
function get_ad($id) {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT * FROM " . DB_TBL_ANUNCIOS;
	$query .= "WHERE id = $id";
	
	$result = sql ( $query );
	
	// Si és correcte torna l'anunci, si no retorna un error
	$return = $result ["result"] [0];
	
	return $return;
}

//Obté una de les categories per mostrar-la al llistat quan es busca per província
function get_ad_category($id){
 	$return = ""; // Valorn que retornarem 
 	
  	$query = "SELECT descripcion 
  			FROM " . DB_TBL_ANUNCIOS_CATEGORIAS . " ac, " . DB_TBL_CATEGORIAS . " a ";
  	$query .= "WHERE id_anuncio = $id 
  				AND ac.id_categoria = a.id LIMIT 1"; 
  	
 	$result = sql ( $query ); 
 
 	// Si és correcte torna la llista de categories, si no retorna un error 
 	$return = $result ["result"]; 
 	
 	return $return; 
}

// Obtenir un llistat d'anuncis actius per categoria
function get_ads_by_category($category, $limit) {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT DISTINCT " . DB_TBL_ANUNCIOS . ".id,
			" . DB_TBL_ANUNCIOS . ".nom,
			" . DB_TBL_ANUNCIOS . ".nom_seo,  
			" . DB_TBL_ANUNCIOS . ".eslogan, 
			" . DB_TBL_ANUNCIOS . ".cp, 
			" . DB_TBL_IMG_ANUNCIOS . ".ruta ";
	$query .= "FROM " . DB_TBL_ANUNCIOS . ", ";
	$query .= DB_TBL_IMG_ANUNCIOS . ", ";
	$query .= DB_TBL_ANUNCIOS_CATEGORIAS . ", ";
	$query .= DB_TBL_CATEGORIAS;
	$query .= "WHERE " . DB_TBL_ANUNCIOS_CATEGORIAS . ".id_categoria = " . DB_TBL_CATEGORIAS . ".id ";
	$query .= "AND " . DB_TBL_ANUNCIOS_CATEGORIAS . ".id_anuncio = ". DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_CATEGORIAS . ".descripcionseo = \"$category\" ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".img_buscador = 1 ";
	$query .= "AND activo = 1 ";
	$query .= "ORDER BY " . DB_TBL_ANUNCIOS . ".caducidad "; //id
	$query .= "DESC LIMIT $limit , 16";
	
	$result = sql ( $query );
	
	// Si és correcte torna la llista d'anuncis de la categoria, si no retorna un error
	$return = $result ["result"];
	
	return $return;
}

// Obtenir un llistat d'anuncis actiu per provincia
function get_ads_by_province($province, $limit) {
	$return = ""; // Valorn que retornarem

	$query = "SELECT DISTINCT " . DB_TBL_ANUNCIOS . ".id,
			" . DB_TBL_ANUNCIOS . ".nom,
			" . DB_TBL_ANUNCIOS . ".nom_seo,
			" . DB_TBL_ANUNCIOS . ".eslogan,
			" . DB_TBL_ANUNCIOS . ".cp,
			" . DB_TBL_IMG_ANUNCIOS . ".ruta ";
	$query .= "FROM " . DB_TBL_ANUNCIOS . ", ";
	$query .= DB_TBL_IMG_ANUNCIOS . ", ";
	$query .= DB_TBL_ANUNCIOS_PROVINCIAS . ", ";
	$query .= DB_TBL_PROVINCIA . " ";
	$query .= "WHERE " . DB_TBL_ANUNCIOS_PROVINCIAS . ".id_provincia = " . DB_TBL_PROVINCIA . ".id ";
	$query .= "AND " . DB_TBL_ANUNCIOS_PROVINCIAS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_PROVINCIA . ".provinciaseo = \"$province\" ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".img_buscador = 1 ";
	$query .= "AND activo = 1 ";
	$query .= " ORDER BY " . DB_TBL_ANUNCIOS . ".caducidad "; //id
	$query .= "DESC LIMIT $limit , 16";

	$result = sql ( $query );

	// Si és correcte torna la llista d'anuncis de la categoria, si no retorna un error
	$return = $result ["result"];

	return $return;
}

// Obtenir un llistat d'anuncis actius per categoria i provincia
function get_ads_by_category_province($category, $province, $limit) {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT DISTINCT " . DB_TBL_ANUNCIOS . ".id,
			" . DB_TBL_ANUNCIOS . ".nom,
			" . DB_TBL_ANUNCIOS . ".nom_seo,
			" . DB_TBL_ANUNCIOS . ".eslogan, 
			" . DB_TBL_ANUNCIOS . ".cp, 
			" . DB_TBL_IMG_ANUNCIOS . ".ruta ";
	$query .= "FROM " . DB_TBL_ANUNCIOS . ", ";
	$query .= DB_TBL_PROVINCIA . ", ";
	$query .= DB_TBL_IMG_ANUNCIOS . ", ";
	$query .= DB_TBL_ANUNCIOS_PROVINCIAS . ", ";
	$query .= DB_TBL_ANUNCIOS_CATEGORIAS . ", ";
	$query .= DB_TBL_CATEGORIAS . " ";
	$query .= "WHERE " . DB_TBL_CATEGORIAS .".descripcionseo = \"$category\" ";
	$query .= "AND " . DB_TBL_PROVINCIA . ".provinciaseo = \"$province\" ";
	$query .= "AND " . DB_TBL_ANUNCIOS_PROVINCIAS . ".id_provincia = " . DB_TBL_PROVINCIA . ".id ";
	$query .= "AND " . DB_TBL_ANUNCIOS_PROVINCIAS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_ANUNCIOS_CATEGORIAS . ".id_categoria = " . DB_TBL_CATEGORIAS . ".id ";
	$query .= "AND " . DB_TBL_ANUNCIOS_CATEGORIAS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".img_buscador = 1 ";
	//$query .= "AND " . DB_TBL_ANUNCIOS_CATEGORIAS . ".id_anuncio = " . DB_TBL_ANUNCIOS_PROVINCIAS . ".id_anuncio ";
	$query .= "AND activo = 1 ";
	$query .= "ORDER BY " . DB_TBL_ANUNCIOS . ".caducidad "; //no estaba al linea
	$query .= "DESC LIMIT $limit , 16 ";
	
	//echo $query;
	
	$result = sql ( $query );
	
	// Si és correcte torna la llista d'anuncis de la categoria per provincia, si no retorna un error
	$return = $result ["result"];
	
	return $return;
}

// Obtenir les imatges d'un anunci
function get_ad_images($ad_id) {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT * FROM " . DB_TBL_IMG_ANUNCIOS;
	$query .= "WHERE id_anuncio = $ad_id ";
	$query .= "AND img_ficha_g = 1";
	
	$result = sql ( $query );
	
	// Si és correcte torna la llista d'imatges, si no retorna un error
	$return = $result ["result"];
	
	return $return;
}

//Obtenir els anuncis de la portada
function get_ads_frontpage($ads_array) {
	$return = ""; // Valorn que retornarem
	
	$in = "";
	foreach ($ads_array as $ad){
		$in .= "$ad,";
	}
	$in = rtrim($in,',');

	$query = "SELECT DISTINCT " . DB_TBL_ANUNCIOS . ".id,
			" . DB_TBL_ANUNCIOS . ".nom,
			" . DB_TBL_ANUNCIOS . ".nom_seo,
			" . DB_TBL_ANUNCIOS . ".eslogan,
			" . DB_TBL_ANUNCIOS . ".cp,
			" . DB_TBL_IMG_ANUNCIOS . ".ruta ,
			" . DB_TBL_IMG_ANUNCIOS . ".ruta_p ";
	$query .= "FROM " . DB_TBL_ANUNCIOS . ", ";
	$query .= DB_TBL_IMG_ANUNCIOS . " ";
	$query .= "WHERE " . DB_TBL_IMG_ANUNCIOS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".img_buscador = 1 ";
	$query .= "AND " . DB_TBL_ANUNCIOS . ".id IN ( $in ) ";
	$query .= "AND activo = 1  ORDER BY ID DESC";

	$result = sql ( $query );

	// Si és correcte torna la llista d'anuncis de la categoria, si no retorna un error
	$return = $result ["result"];

	return $return;
}


//Obtenir el count dels anuncis
function get_ads_count($cat, $prov) { 
 	$return = ""; // Valorn que retornarem 
 	
	$query = "SELECT count(*) AS \"count\" ";
	$query .= "FROM " . DB_TBL_ANUNCIOS . ", ";
	if ($prov != "espana"){
		$query .= DB_TBL_PROVINCIA . ", ";
		$query .= DB_TBL_ANUNCIOS_PROVINCIAS . ", ";
	}
	if ($cat != "bodas"){
		$query .= DB_TBL_CATEGORIAS . ", ";
		$query .= DB_TBL_ANUNCIOS_CATEGORIAS . ", ";
	}
	$query .= DB_TBL_IMG_ANUNCIOS . " ";
	$query .= "WHERE activo = 1 ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".img_buscador = 1 ";
	if ($prov != "espana" && $cat == "bodas"){
		$query .= "AND " . DB_TBL_ANUNCIOS_PROVINCIAS . ".id_provincia = " . DB_TBL_PROVINCIA . ".id ";
		$query .= "AND " . DB_TBL_ANUNCIOS_PROVINCIAS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
		$query .= "AND " . DB_TBL_PROVINCIA . ".provinciaseo = \"$prov\" ";
	} elseif ($cat != "bodas" && $prov == "espana"){
		$query .= "AND " . DB_TBL_ANUNCIOS_CATEGORIAS . ".id_categoria = " . DB_TBL_CATEGORIAS . ".id ";
		$query .= "AND " . DB_TBL_ANUNCIOS_CATEGORIAS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
		$query .= "AND " . DB_TBL_CATEGORIAS .".descripcionseo = \"$cat\" ";
	} elseif ($cat != "bodas" && $prov != "espana"){
		$query .= "AND " . DB_TBL_ANUNCIOS_CATEGORIAS . ".id_categoria = " . DB_TBL_CATEGORIAS . ".id ";
		$query .= "AND " . DB_TBL_ANUNCIOS_CATEGORIAS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
		$query .= "AND " . DB_TBL_CATEGORIAS .".descripcionseo = \"$cat\" ";
		$query .= "AND " . DB_TBL_ANUNCIOS_PROVINCIAS . ".id_provincia = " . DB_TBL_PROVINCIA . ".id ";
		$query .= "AND " . DB_TBL_ANUNCIOS_PROVINCIAS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
		$query .= "AND " . DB_TBL_PROVINCIA . ".provinciaseo = \"$prov\" ";
	}
	
 	$result = sql ( $query ); 
 
 	// Si és correcte torna la llista de categories, si no retorna un error 
 	if ($result["ok"]){
 		$return = $result ["result"][0]["count"];
 	} else {
 		$return = $result ["result"];
 	}
 	
 	return $return; 
 }

/*
 * Funcions de les provincies i els municipis
 */

// Obtenir la llista de provincies
function get_provincies() {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT id, provincia, provinciaseo FROM " . DB_TBL_PROVINCIA;
	$query .= "ORDER BY provincia ";
	$result = sql ( $query );
	
	// Si és correcte torna la llista de provincies, si no retorna un error
	$return = $result ["result"];
	
	return $return;
}

// Obtenir un municipi
function get_municip($pc) {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT * FROM " . DB_TBL_POBLACION;
	$query .= "WHERE postal = $pc ";
	
	$result = sql ( $query );
	
	// Si és correcte torna el municipi, si no retorna un error
	$return = $result ["result"] [0];
	
	return $return;
}

// Obtenir una provincia
function get_province($id) {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT * FROM " . DB_TBL_PROVINCIA;
	$query .= "WHERE id = $id ";
	
	$result = sql ( $query );
	
	// Si és correcte torna la provincia, si no retorna un error
	$return = $result ["result"] [0];
	
	return $return;
}

// Obtenir una provincia SEO
function get_province_seo($seo) {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT provincia FROM " . DB_TBL_PROVINCIA;
	$query .= "WHERE provinciaseo = \"$seo\" ";
	
	$result = sql ( $query );
	
	// Si és correcte torna la provincia, si no retorna un error
	if ($result ["ok"]) {
		$return = $result ["result"] [0] ["provincia"];
	} else {
		$return = $result ["result"];
	}
	
	return $return;
}

/*
 * Funcions de SQL
 */

// Llença una consulta contra la BBDD
function sql($query) {
	$result ["ok"] = false; // Asumim error per defecte
	$result ["result"] = ""; // Resultat en cas que vagi bé i sigui un select o error si és un error
	
	if ($sql = mysql_query ( $query )) {
		$result ["ok"] = true;
		if (substr ( $query, 0, 6 ) == "SELECT") {
			if (mysql_num_rows ( $sql ) > 0) {
				$result ["result"] = fetch ( $sql );
			} else {
				$result ["ok"] = false;
				$result ["result"] = error ( $sql );
				$result ["sql"] = $query;
			}
		}
	} else {
		$result ["result"] = error ( $sql );
		$result ["sql"] = $query;
	}
	return $result;
}

// Fetch d'un select
function fetch($sql) {
	$return_fetch = "";
	
	while ( $fetch = mysql_fetch_assoc ( $sql ) ) {
		$return_fetch [] = $fetch;
	}
	
	return $return_fetch;
}

// Control d"errors d'SQL
function error($sql) {
	$return = "";
	if (mysql_errno () > 0) {
		$return = "Error " . mysql_errno () . ": " . mysql_error ();
	} else {
		$return = "Error " . mysql_errno () . ": Empty set";
	}
	
	return $return;
}

/*
 * Funcions SEO
 */
function get_category_title($seo) {
}

function get_category_description($seo){
	
}

function get_category_keywords($seo){
	
}
/*
 * Final funcions SEO
 */

/*
 * Funcions Auxiliars
 */

// Obtenir la img del buscador d'una img
// També torna les ruta generica de les imatges
function get_ad_img_home($ad_id, $path) {
	return "img/anuncios/" . $ad_id . "/" . $path;
}

// Obtenir el banner d'un anunci
function get_add_banner($ad_id) {
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT ruta FROM " . DB_TBL_IMG_ANUNCIOS;
	$query .= "WHERE id_anuncio = $ad_id ";
	$query .= "AND img_banner = 1";
	
	$result = sql ( $query );
	
	// Si és correcte torna la llista de categories, si no retorna un error
	$return = "img/anuncios/" . $ad_id . "/" . $result ["result"] [0] ["ruta"];
	
	return $return;
}

// Marcar un select en cas que els values siguin iguals
function is_selected($value, $compare) {
	if ($value == $compare) {
		print "selected=\"selected\"";
	}
}

//Obtenir el id de la categoria a partir del seu nom SEO
function get_description_id($descriptionseo) {
	$return = "";
	
	$query = "SELECT id FROM " . DB_TBL_CATEGORIAS;
	$query .= "WHERE descripcionseo = $descriptionseo ";
	
	$result = sql ( $query );
	$return = $result ["result"] [0] ["id"];
	return $return;
}

//Obtenir el id de la provincia a partir del seu nom SEO 
function get_province_id($provinceseo) {
	$return = "";
	
	$query = "SELECT id FROM " . DB_TBL_PROVINCIA;
	$query .= "WHERE descripcionseo = $descriptionseo ";
	
	$result = sql ( $query );
	$return = $result ["result"] [0] ["id"];
	return $return;
}

//Obtenir la llista de XXSS vinculades al anunci
function get_ad_social_networks($id) { 
 	$return = ""; // Valorn que retornarem 
 	
  	$query = "SELECT enlace, imagen ";
  	$query .= "FROM " . DB_TBL_ANUNCIOS_RRSS;
  	$query .= ", " . DB_TBL_REDES_SOCIALES;
  	$query .= "WHERE " . DB_TBL_ANUNCIOS_RRSS . ".id_anuncio = $id ";
  	$query .= "AND " . DB_TBL_ANUNCIOS_RRSS . ".id_red_social = " . DB_TBL_REDES_SOCIALES . ".id"; 
 	$result = sql ( $query ); 
 
 	// Si és correcte torna la llista de categories, si no retorna un error 
 	$return = $result ["result"]; 
 	
 	return $return; 
}
 
 //Obtenir els anuncis recomenats
 function get_recommended_ads() { 
 	$return = ""; // Valorn que retornarem 
 	
  	$query = "SELECT " . DB_TBL_ANUNCIOS_RECOMENDADOS . ".orden, 
  			" . DB_TBL_ANUNCIOS . ".id, 
  			" . DB_TBL_ANUNCIOS . ".eslogan,
  			" . DB_TBL_ANUNCIOS . ".nom,
  			" . DB_TBL_ANUNCIOS . ".nom_seo,
  			" . DB_TBL_ANUNCIOS . ".cp, 
  			" . DB_TBL_IMG_ANUNCIOS . ".ruta ";
	$query .= " FROM " . DB_TBL_ANUNCIOS_RECOMENDADOS . ", ";
	$query .= DB_TBL_ANUNCIOS . ", ";
	$query .= DB_TBL_IMG_ANUNCIOS;
	$query .= "WHERE ". DB_TBL_ANUNCIOS_RECOMENDADOS . ".anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".id_anuncio = " . DB_TBL_ANUNCIOS . ".id ";
	$query .= "AND " . DB_TBL_IMG_ANUNCIOS . ".img_buscador = 1 ";
  	$query .= "ORDER BY orden";
  	 
 	$result = sql ( $query ); 
 
 	// Si és correcte torna la llista de categories, si no retorna un error 
 	$return = $result ["result"]; 
 	
 	return $return; 
}
 
function get_repo($id) {
	$return = ""; // Valorn que retornarem

	$query = "SELECT * FROM " . DB_TBL_REPORTAJES;
	$query .= "WHERE id = $id";

	$result = sql ( $query );

	// Si és correcte torna l'anunci, si no retorna un error
	$return = $result ["result"] [0];

	return $return;
}

function get_repo_seo($seo) {
	$return = ""; // Valorn que retornarem

	$query = "SELECT * FROM " . DB_TBL_REPORTAJES;
	$query .= "WHERE titulo_seo = \"$seo\"";

	$result = sql ( $query );

	// Si és correcte torna l'anunci, si no retorna un error
	$return = $result ["result"];

	return $return;
}

function get_repos() {
	$return = ""; // Valorn que retornarem

	$query = "SELECT * FROM " . DB_TBL_REPORTAJES;

	//save_log ( $query, 1 );
	$result = sql ( $query );

	$return = $result ["result"];


	return $return;
}

function get_fira($id) {
	$return = ""; // Valorn que retornarem

	$query = "SELECT * FROM " . DB_TBL_FERIAS;
	$query .= "WHERE id = $id";

	$result = sql ( $query );

	$return = $result ["result"] [0];

	return $return;
}

function seoUrl($str){
	/** by Jonatas Urias B Teixeira **/
	$a = array('/(à|á|â|ã|ä|å|æ)/','/(è|é|ê|ë)/','/(ì|í|î|ï)/','/(ð|ò|ó|ô|õ|ö|ø|œ)/','/(ù|ú|û|ü)/','/ç/','/þ/','/ñ/','/ß/','/(ý|ÿ)/','/(=|\+|\/|\\\|\.|\'|\_|\\n| |\(|\))/','/[^a-z0-9_ -]/s','/-{2,}/s');
	$b = array('a','e','i','o','u','c','d','n','ss','y','-','','-');
	return trim(preg_replace($a, $b, strtolower($str)),'-');
}

function show_firas(){
	$return = ""; // Valorn que retornarem
	
	$query = "SELECT * FROM " . DB_TBL_FERIAS;
	
	$result = sql ( $query );
	if($result["ok"]=="ok"){
		$result = $result["result"];
	}else{
		return $return["result"];
	}
	
	$return .= "<table border='3px' >";
	for($i=0;$i<sizeof($result);$i++){
		if($i%2==0){
			$return .= "<tr>";
		}
		//echo $result[$i]["nombre"]; //estoooo 
		$url = strpos($result[$i]["url"], "http://");
		$return .= "<td><div class='fira'>";
		$return .= "<img src='img/ferias/". $result[$i]["img"] ."' alt='". $result[$i]["img"] ."'>";
		$return .= "<div class='fira_nom'>". $result[$i]["nombre"] ."</div>";
		$return .= "<div class='fira_desc'>". $result[$i]["descripcion"] ."</div>";
		$return .= "<div class='fira_dir'>". $result[$i]["direccion"] ."</div>";
		if (!$url){
			$return .= "<div class='fira_url'><a href='http://". $result[$i]["url"] ."'>". $result[$i]["url"] ."</a></div>";
		} else {
			$return .= "<div class='fira_url'><a href='". $result[$i]["url"] ."'>". $result[$i]["url"] ."</a></div>";
		}
		$return .= "</div></td>";
		if($i%2==1){
			$return .= "</tr>";
		}
	}
	$return .= "</table>";
	return $return;
}

function show_4_firas(){
	$return = ""; // Valorn que retornarem

	$query = "SELECT * FROM " . DB_TBL_FERIAS . "order by id desc limit 4";

	$result = sql ( $query );
	if($result["ok"]=="ok"){
		$result = $result["result"];
	}else{
		return $return["result"];
	}

	
	for($i=0;$i<sizeof($result);$i++){
		
		$url = strpos($result[$i]["url"], "http://");
		$return .= "<div class='fira_recuadre'>";
		$return .= "<a href='feria/" . $result[$i]["id"] ."/". seoUrl($result[$i]["nombre"]) ."' title='". $result[$i]["nombre"] ."'>
						<img width='152' height='95' src='img/ferias/". $result[$i]["img"] ."' alt='". $result[$i]["nombre"] ."' title='". $result[$i]["nombre"] ."'>
					</a>";
		//$return .= "<a href='http://". $result[$i]["id"] ."'>ID=". $result[$i]["id"] ."</a>";

		$return .= "</div>";
		
	}

	return $return;
}

function get_ads($ads){
	$query ="";
	$return = array();
	foreach($ads as $ad){
		$query = "SELECT * FROM " .DB_TBL_ANUNCIOS. " WHERE id = " .$ad;
		$result = sql ( $query );
		$return[$ad] = $result["result"][0];
	}
	return $return;
}
function get_ads_img($ads){
	$return = array();
	foreach($ads as $ad){
		$query = "SELECT ruta FROM " .DB_TBL_IMG_ANUNCIOS. " WHERE id_anuncio = " .$ad. " AND img_buscador = 1";
		$result = sql ( $query );
		$return[$ad] = $result["result"][0];
	}
	return $return;
}

?>