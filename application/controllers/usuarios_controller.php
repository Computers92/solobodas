<?php
                        //extendemos CI_Controller

class usuarios_controller extends CI_Controller{

    public function __construct() {

        //llamamos al constructor de la clase padre

        parent::__construct();

         

        //llamo al helper url
        $this->load->helper("url"); 

         

        //llamo o incluyo el modelo

        $this->load->model("usuarios_model");

         

        //cargo la libreria de sesiones

        $this->load->library("session");

    }

     

    //controlador por defecto

    public function index(){

        

        //array asociativo con la llamada al metodo
        //del modelo
        $usuarios["ver"]=$this->usuarios_model->ver();

         
        //cargo la vista y le paso los datos

        $this->load->view("usuarios_view",$usuarios);

    }
