<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recomendados extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
 
        //cargamos la base de datos por defecto
        $this->load->database('default');
        //cargamos el helper url y el helper form
        $this->load->helper(array('url','form'));
        //cargamos la librería form_validation
        $this->load->library(array('form_validation'));
        //cargamos el modelo crud_model
        $this->load->model('crud_model');
 
    }
 
    //cargamos la vista y pasamos el título y los usuarios a
    //través del array data a la misma
    public function index()
    {
		 $data = array(
                      'recomendados' => $this->crud_model->get_recomendados(),


					  );
 
        $this->load->view('recomendados',$data);
    }


	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */