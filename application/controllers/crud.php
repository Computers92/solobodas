<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Crud extends CI_CONTROLLER
{
    public function __construct()
    {
        parent::__construct();
 
        //cargamos la base de datos por defecto
        $this->load->database('default');
        //cargamos el helper url y el helper form
        $this->load->helper(array('url','form'));
        //cargamos el modelo crud_model
        $this->load->model('crud_model');
 
    }
 
    //cargamos la vista y pasamos el título y los usuarios a
    //través del array data a la misma
    public function index()
    {
 		$this->form_validation->set_message('categoria', 'You must select a business');
        $data = array('titulo' => 'Crud en codeigniter',
					  'users' => $this->crud_model->get_users(),
					  'anuncios' => $this->crud_model->get_anuncios(),
					  'categorias'=>$this->crud_model->get_categorias(),
					  'provincias'=>$this->crud_model->get_provincias(),

					  );
        $this->load->view('crud',$data);
    }
 
  
}
 
/*
*Location: application/controllers/crud.php
*/