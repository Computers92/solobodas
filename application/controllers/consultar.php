<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consultar extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
 
        //cargamos la base de datos por defecto
        $this->load->database('default');
        //cargamos el helper url y el helper form
        $this->load->helper(array('url','form'));
        //cargamos la librería form_validation
        $this->load->library(array('form_validation'));
        //cargamos el modelo crud_model
        $this->load->model('crud_model');
 
    }
 
    //cargamos la vista y pasamos el título y los usuarios a
    //través del array data a la misma
    public function index()
    {
		
		$categoria =$this->input->post('categoria');
		$provincia =$this->input->post('provincia');
		$data = array(
		    'anuncios' => $this->crud_model->get_consultas($categoria,$provincia),
		);
	        $this->load->view('consultar',$data);
    }
	public function  anuncio(){
		$id=$this->uri->segment(2);
		$data = array(
			'show_info' => $this->crud_model->get_info($id),
		    'show_photos' => $this->crud_model->get_info_anuncio($id),
			'show_banner' => $this->crud_model->get_banner($id),
		);
		$this->load->view('anuncios_datos',$data);
	}


	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */