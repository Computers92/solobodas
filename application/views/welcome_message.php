<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Solobodas</title>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/mas.css" rel="stylesheet">

	<link href="assets/css/estilos.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <div class="container-fluid">	
	
	<div class='row fondobanner' >
				<div class="col-xs-12 navbar-fixed-top navbar">
					
					<img src="assets/img/solobodas.png" class="img-responsive centrar" alt="Solobodas">
				</div>
				<div class="col-xs-12 margenbanner" >
				<form action="consultar" method="POST">
				<div class="col-xs-12 que">
					Selecciona tus preferencias
				</div>
				<div class='col-xs-12'>
					<select name="categoria" class="form-control margen">
						<?php
							echo '<option value="100">Selecciona una categoría</option>';
							foreach($categorias as $i => $categoria)
								echo '<option value="'.$categoria->descripcionseo.'">'.$categoria->descripcion.'</option>';
						?>
					</select>
				</div>
				
					<div class='col-xs-12 s'>
						<select name="provincia" class="form-control margen">
						<?php
							echo '<option value="100">Selecciona una provincia</option>';
							foreach($provincias as $i => $provincia)
								echo '<option value="'.$provincia->provinciaseo.'">'.$provincia->provincia.'</option>';
						?>
					</select>
				</div>
				<div class="col-xs-12 margen_submit" >
					<input type="submit" id="singleton" name="singlebutton" class="btn enviar center-block " value="Buscar"></input>		
				</div>
				</form>
				</div>
		</div>
		<div class="row">
					<div class="col-xs-12 nopadding  ">
						<p class="design1" ><a href="recomendados" title="recomendados">Ver recomendados</a></p>
					</div>
					
		</div>

			<div class="row" >
			<?php
            foreach($users as $fila):
            ?>
 
				<a href="anuncio/<?php echo $fila->id?>" class="enlace"><div class="col-xs-23 col-sm-15 col-md-15 nopadding" style="margin-bottom:1%;margin-top:1%;">
				<div class="col-xs-12 col-md-12 nopadding thumbail">
								 <div class="jumbtron ratio" style="background-image:url('http://www.solobodas.net/img/anuncios/<?php echo $fila->id?>/<?php echo $fila->ruta?>')">

									</div>
								<div class=" col-xs-12 caption post-content nopadding sobreponer transparente">
										<div class="col-xs-12 nopadding titulo">
											<?php echo $fila->nom ?>	
											</div>
										<div class="col-xs-12 nopadding categoria2">
											<?php echo $fila->descripcion ?>												
										</div>
									</div>
									
				</div>
				
			</div></a>
		<?php
			endforeach;
         ?>			
						</div>
	</div>
	
	<div class="row nopadding nomargen" >
		<?php
            foreach($anuncios as $fila):
            ?>
			<a href="anuncio/<?php echo $fila->id?>"><div class="col-xs-15 col-md-18 nopadding margen">
			<div class="col-xs-20 col-md-6 col-sm-6 nopadding">
				<div class="col-xs-12 col-md-12 nopadding nomargen thumbail">
								
								 <div class="ratio" style="background-image:url('http://www.solobodas.net/img/anuncios/<?php echo $fila->id?>/<?php echo $fila->ruta_p ?>')">
									</div>
									
					
				</div>
				
			</div>
			<a href="anuncio/<?php echo $fila->id?>"><div class="nopadding col-xs-20 col-md-6 col-sm-6  ">
					<div class=" col-xs-12 ">
									<div class="col-xs-12 nopadding titulo">
											<?php echo $fila->nom?>										
									</div>
										<div class="col-xs-12 nopadding categoria2">
											<?php echo $fila->descripcion?>
										</div>
						 				<div class="col-xs-12 nopadding provincia2">
									
											<span class="provincia"><?php echo $fila->poblacion?> | <?php echo $fila->provincia?></span>										
										</div>
									
										
									</div>
					<div class="col-xs-12 col-md-12  nomargen sobresale eslogan">
						<?php echo $fila->eslogan?>
					</div>
				</div>
				
			</div></a>
		<?php
			endforeach
		?>
					
					</div>
	<div class="row nopadding nomargen">
					<div class="footer ">
						<a title="solobodas" class="enlacefooter" href="solobodas.net" style="color:white;">Solobodas.net</a> | <a class="enlacefooter nodecoration" style="color:white;" href="http://solobodas.net/Condiciones%20Legales%20de%20Solobodas.pdf" title="Condiciones legales">Condiciones legales</a>

					</div>
				
		</div>
		
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
	<script type="assets/text/javascript" src="js/config.js"></script>
	<script type="text/javascript">
		// jQuery to collapse the navbar on scroll
		$(window).scroll(function() {
			if ($(".navbar").offset().top > 50) {
				$(".navbar-fixed-top").addClass("top-nav-collapse");
				
			} else {
				$(".navbar-fixed-top").removeClass("top-nav-collapse");
			}
		});
	</script>
  </body>
</html>
