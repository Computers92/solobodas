<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Solobodas</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../assets/css/mas.css" rel="stylesheet">

	<link href="../assets/css/estilos.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<div class="container-fluid nopadding nomargen">
	<div class='row container centrar-contenido nopadding nomargen'>
				
				<div class="col-xs-12 navbar-fixed-top navbar">
					
					<img src="../assets/img/solobodas.png" class="img-responsive centrar" alt="Solobodas">
				</div>
		</div>
		
		<div class="row container centrar-contenido centrar-texto nopadding nomargen">
			
					
					<div class="col-xs-12 col-md-12 col-lg-12 cabecera negrita nopadding margen_anuncio">
							<?php echo $show_info['nom'] ?>
					</div>			
	
					<div class="col-xs-12 col-md-12 col-lg-12 nopadding nomargen">
						<img src="http://solobodas.net/img/anuncios/<?php echo $show_banner['id_anuncio'] ?>/<?php echo $show_banner['ruta'] ?>" class="img-responsive centrar ancho"  alt="anuncio">
					</div>
					
					<div class="col-xs-12 col-md-10 col-lg-10 nopadding nomargen" style="text-align:justify;padding-left:15px;padding-right:15px;margin-top:8px;" >
							<?php echo $show_info['descripcion'] ?>
					</div>
					<div class="col-xs-12 col-md-2 col-lg-2 nomargen nopadding">
							
							<div class="col-xs-12 col-md-12 col-lg-12">
								<div class="negrita">Telefono</div>
								<p>
								<?php echo $show_info['telefono'] ?>
							</div>
							<div class="col-xs-12 col-md-12 col-lg-12">
								<div class="negrita">Mail</div>
								<p>
								<?php echo $show_info['mail'] ?>
							</div>
							<div class="col-xs-12 col-md-12 col-lg-12">
								<div class="negrita">Direccion</div>
								<p>
								<?php echo $show_info['direccion'] ?>
							</div>
					</div>
					<div class="col-xs-12 col-md-12 maps" style="padding:0;margin:0;">
							<?php echo $show_info['gmaps'] ?>
					</div>

					<?php
					foreach($show_photos as $anuncio){
					?>	
						<div class="col-xs-22 col-md-20 maps nopadding" style="margin-bottom:0.5%;" align="center">
							<a title="imagen" href="http://www.solobodas.net/img/anuncios/<?php echo $anuncio->id_anuncio?>/<?php echo $anuncio->ruta?>"><img src="http://www.solobodas.net/img/anuncios/<?php echo $anuncio->id_anuncio?>/<?php echo $anuncio->ruta_p?>" class="img-responsive imgcentrar"></a>
						</div>
					<?php
					}
					?>
		</div>
		
			<div class="row nopadding nomargen">
					<div class="footer ">
						<a title="solobodas" class="enlacefooter" href="solobodas.net" style="color:white;">Solobodas.net</a> | <a class="enlacefooter nodecoration" style="color:white;" href="http://solobodas.net/Condiciones%20Legales%20de%20Solobodas.pdf" title="Condiciones legales">Condiciones legales</a>

					</div>
				
		</div>
				
		</div>
	</div>
		
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
	<script type="assets/text/javascript" src="js/config.js"></script>
	<script type="text/javascript">
		// jQuery to collapse the navbar on scroll
		$(window).scroll(function() {
			if ($(".navbar").offset().top > 50) {
				$(".navbar-fixed-top").addClass("top-nav-collapse");
				
			} else {
				$(".navbar-fixed-top").removeClass("top-nav-collapse");
			}
		});
	</script>
  </body>
</html>
