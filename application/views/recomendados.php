<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Solobodas</title>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/mas.css" rel="stylesheet">

	<link href="assets/css/estilos.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <div class="container-fluid">	
	
		<div class='row fondobanner' >
				<div class="col-xs-12 navbar-fixed-top navbar">
					
					<img src="assets/img/solobodas.png" class="img-responsive centrar" alt="Solobodas">
				</div>
		</div>

			<div class="row nopadding margen_consulta ">
		<?php
            foreach($recomendados as $fila):
            ?>
		<a href="anuncio/<?php echo $fila->id?>"><div class="col-xs-15 col-md-18 nopadding margen">
			<div class="col-xs-20 col-md-6 col-sm-6 nopadding">
				<div class="col-xs-12 col-md-12 nopadding nomargen thumbail">
								
								 <div class="ratio" style="background-image:url('http://www.solobodas.net/img/anuncios/<?php echo $fila->id?>/<?php echo $fila->ruta_p ?>')">
									</div>
									
					
				</div>
				
			</div>
			<div class="col-xs-20 col-md-6 col-sm-6 nopadding nomargen">
					<div class=" col-xs-12 nopadding">
									<div class="col-xs-12 nopadding titulo">
											<?php echo $fila->nom?>										
									</div>
										<div class="col-xs-12 nopadding categoria2">
											<?php echo $fila->descripcion?>
										</div>
						 				
										<div class="col-xs-12 nopadding provincia2">
									
											<span class="provincia"><?php echo $fila->poblacion?> | <?php echo $fila->provincia?></span>										
										</div>
									</div>
					<div class="col-xs-12 col-md-12 nopadding nomargen sobresale eslogan">
											<?php echo $fila->eslogan?>										
			</div>
				</div>
				
			</div></a>	
		<?php
			endforeach
		?>
					
					</div>			
	</div>
	<div class="row nopadding nomargen">
					<div class="footer ">
						<a title="solobodas" class="enlacefooter" href="solobodas.net" style="color:white;">Solobodas.net</a> | <a class="enlacefooter nodecoration" style="color:white;" href="http://solobodas.net/Condiciones%20Legales%20de%20Solobodas.pdf" title="Condiciones legales">Condiciones legales</a>

					</div>
				
		</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
	<script type="assets/text/javascript" src="js/config.js"></script>
	<script type="text/javascript">
		// jQuery to collapse the navbar on scroll
		$(window).scroll(function() {
			if ($(".navbar").offset().top > 50) {
				$(".navbar-fixed-top").addClass("top-nav-collapse");
				
			} else {
				$(".navbar-fixed-top").removeClass("top-nav-collapse");
			}
		});
	</script>
  </body>
</html>
